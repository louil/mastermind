#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void get_input(int list1[4], int *red, int *white);

int main(void){
	
	int i = 0;
 	int red = 0, white = 0;
	int list1[4] = {0};
	
	//Creating a seed for random
	srand(time(NULL));													
	
	//Creating 4 random numbers to be put into list1, representing the number to be guessed
	for(i = 0; i < 4; i++){
		list1[i] = (rand()%10);
		printf("%d", list1[i]);
	}
	printf("\n");
	printf("Guess a number: ");
	get_input(list1, &red, &white);
}

void get_input(int *list1, int *red, int *white){

	//Creating variables
	int guesscount = 0, s = 0; 
	char rand[5] = {0};
	int hope[5] = {0}, hopecopy[5] = {0}, list1copy[5] = {0};

	while(1){	

		fgets(rand, (sizeof(rand) + 1), stdin);

		//Check each individual part of rand to make sure they are digits
		for (s = 0; s < 4; s++){
			if (rand[s] >= '0' && rand[s] <= '9'){
				;
			}
			else{
				while(!strchr(rand, '\n')){
					fgets(rand, (sizeof(rand) + 1), stdin);
				}
				goto beggining;
			}
		}
	
		//Check to make sure input is valid
		if (rand[4] != '\n' || strlen(rand) < 4){
			printf("That was not 4 numbers.\n");
			while(!strchr(rand, '\n')){
				fgets(rand, (sizeof(rand) + 1), stdin);
			}
			goto beggining;
		}
		else if (rand[4] == '\n'){
			
		}
		else{
			printf("Wrong value.\n");
			goto beggining;
		}

		//Convert input into long
		long answer = strtol(rand, NULL, 10);	
	
		for (int p = 3; p >= 0; --p) {
			hope[p] = answer %10;
  			answer = answer / 10;
		}
		//Creating copy of hope into hopecopy
		for (int q = 0; q < 4; q++){
			hopecopy[q]	= hope[q];
		}
		//Creating copy of list1 into list1copy
		for (int t = 0; t < 4; t++){
			list1copy[t] = list1[t];
		}
		//Loop for red check
		for (int i = 0; i < 4; i++){
			if (hope[i] == list1[i]){
				hopecopy[i] = 11;
				list1copy[i] = 11;
			}
		}
		//Loop for white check
		for (int i = 0; i < 4; i++){
			for (int z = 0; z < 4; z++){
				if (hope[z] == list1copy[i] && z != i && hopecopy[z] != 11 && hopecopy[z] != 12){ 					
					hopecopy[z] = 12;
					list1copy[i] = 12;
				}	
			}
		}
		//Inrement red or white
		for (int g = 0; g < 4; g++){
			if (hopecopy[g] == 11){
				*red += 1;
			}
			else if (hopecopy[g] == 12){
				*white += 1;
			}
		}

		printf("\n%d red\n", *red);
		printf("%d white\n", *white);
		guesscount += 1;

		//If all red then it is a win, print out the number of guesses needed to win then break out, otherwise re-enter the loop
		if (*red == 4){	
			if(guesscount == 1){
				printf("You win! It took you %d valid guess.\n", guesscount);
				break;
			}
			else{
				printf("You win! It took you %d valid guesses.\n", guesscount);	
				break;
			}
		}
		//Send everything back through the loop if this part is reached
		beggining:

			*red = 0;
			*white = 0;
			printf("Guess a number: ");	
	}
}



